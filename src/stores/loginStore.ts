import { ref, onMounted } from 'vue'
import { defineStore } from 'pinia'
import { useRouter } from 'vue-router'

import {
  getAuth,
  createUserWithEmailAndPassword,
  signInWithEmailAndPassword,
  GoogleAuthProvider,
  signInWithPopup,
  UserCredential,
  onAuthStateChanged,
  signOut
} from 'firebase/auth'

export const useUserStore = defineStore('user', () => {
  const email = ref('')
  const password = ref('')
  const router = useRouter()
  const userAuth = ref<UserCredential | null>(null)
  const name = ref<string | undefined>('')

  const register = () => {
    createUserWithEmailAndPassword(getAuth(), email.value, password.value)
      .then((Credentials) => {
        userAuth.value = Credentials
        name.value = Credentials?.user.displayName || undefined
        console.log('Successfully registered!')
        router.push('/about')
      })
      .catch((error) => {
        console.log(error.message)
        alert(error.message)
      })
  }
  const registerGoogle = () => {
    const provider = new GoogleAuthProvider()
    signInWithPopup(getAuth(), provider)
      .then((credentialsUser) => {
        userAuth.value = credentialsUser
        name.value = credentialsUser.user?.displayName || undefined
        console.log('Successfully registered with google!')
        router.push('/about')
      })
      .catch((error) => {
        console.log(error.message)
      })
  }
  const signIn = () => {
    signInWithEmailAndPassword(getAuth(), email.value, password.value)
      .then((credentialsUser) => {
        userAuth.value = credentialsUser
        name.value = credentialsUser.user?.displayName || undefined
        console.log('Successfully logged in!')
        router.push('/about')
      })
      .catch((error) => {
        console.log(error.message)
        alert(error.message)
      })
  }
  const isLogged = ref(false)

  onMounted(() => {
    onAuthStateChanged(getAuth(), (user) => {
      if (user) {
        isLogged.value = true
      } else {
        isLogged.value = false
      }
    })
  })

  const handleSingOut = () => {
    signOut(getAuth()).then(() => {
      router.push('/')
    })
  }
  return {
    email,
    password,
    router,
    userAuth,
    isLogged,
    name,
    register,
    registerGoogle,
    signIn,
    handleSingOut
  }
})
