import { ref, computed, onMounted } from 'vue'
import { defineStore } from 'pinia'
import {
  collection,
  getDocs,
  setDoc,
  doc,
  where,
  query,
  updateDoc,
  deleteDoc
} from 'firebase/firestore'
import { auth, db } from '../firebase'

export const useCounterStore = defineStore('counter', () => {
  let id = 0
  let index: number
  const newTodo = ref('')
  const newTodoName = ref('')
  const hideCompleted = ref(false)
  const changeTodo = ref(false)

  interface todo {
    id: number
    done: boolean
    text: string
    Edit: boolean
  }
  const todos = ref<todo[]>([])
  const editTask = ref(false)
  const currentUser = auth.currentUser
  let userID = ''

  if (currentUser) {
    userID = currentUser.uid
    console.log("Current's user ID: " + userID)
  } else {
    console.log('No user currently logged in')
  }
  const TasksCollectionRef = query(collection(db, 'tasks'), where('UsersID', '==', userID))

  const onSuccess = (docs) => {
    if (Array.isArray(docs)) {
      todos.value = docs.map((item) => item.data())
      id = todos.value.reduce((max, todo) => {
        return todo.id > max ? todo.id : max
      }, 0)
    }
  }

  onMounted(() => {
    getDocs(TasksCollectionRef)
      .then((snapshot) => {
        const docs = snapshot.docs
        onSuccess(docs)
      })
      .catch((error) => {
        console.error('Error retrieving documents:', error)
      })
  })

  const rules = ref([
    (value) => {
      if (value.length === 0) return true
      if (value.length > 3) return true
      return 'Wprowadz przynajmniej 4 znaki'
    }
  ])

  const filteredTodos = computed(() => {
    return hideCompleted.value ? todos.value.filter((t) => !t.done) : todos.value
  })

  const uncompletedCount = computed(() => {
    return todos.value.filter((t) => !t.done).length
  })

  async function addTodo() {
    if (newTodo.value.length < 4) return
    const newTask: todo = {
      id: id,
      text: newTodo.value,
      done: false,
      Edit: false
    }
    todos.value.push(newTask)

    await setDoc(doc(collection(db, 'tasks')), {
      id: id,
      text: newTodo.value.toUpperCase(),
      done: false,
      UsersID: userID,
      Edit: false
    })
    id++
    newTodo.value = ''
    console.log(todos)
  }

  async function removeTodo(todo) {
    if (changeTodo.value) return

    const q = query(TasksCollectionRef, where('UsersID', '==', userID), where('id', '==', todo.id))
    getDocs(q)
      .then((querySnapshot) => {
        const currentDoc = querySnapshot.docs[0]
        const documentRef = doc(db, 'tasks', currentDoc.id)

        deleteDoc(documentRef)
          .then(() => {
            console.log('Document successfully deleted!')
          })
          .catch((error) => {
            console.error('Error deleting document:', error)
          })
      })
      .catch((error) => {
        console.error('Error retrieving documents:', error)
      })

    todos.value = todos.value.filter((t) => t !== todo)
  }
  function editTodo(todo) {
    if (changeTodo.value) {
      return
    }
    changeTodo.value = !changeTodo.value
    todo.Edit = !todo.Edit
    newTodoName.value = todo.text
    index = todo.id
  }
  async function updateTodo() {
    todos.value.forEach((todo) => {
      if (todo.id == index) {
        todo.text = newTodoName.value
        todo.Edit = false
      }
    })
    const tempName = newTodoName.value
    newTodoName.value = ''
    changeTodo.value = false
    const q = query(TasksCollectionRef, where('UsersID', '==', userID), where('id', '==', index))
    getDocs(q)
      .then((querySnapshot) => {
        const currentDoc = querySnapshot.docs[0]
        const documentRef = doc(db, 'tasks', currentDoc.id)
        const updateData = {
          text: tempName
        }

        updateDoc(documentRef, updateData)
          .then(() => {
            console.log('Document successfully updated!')
          })
          .catch((error) => {
            console.error('Error updating document:', error)
          })
      })
      .catch((error) => {
        console.error('Error retrieving documents:', error)
      })
  }

  function sortTodo() {
    todos.value = todos.value.sort((a, b) => a.text.localeCompare(b.text))
  }
  function updateStatus(todo) {
    todo.done = !todo.done
  }
  return {
    updateTodo,
    updateStatus,
    sortTodo,
    editTodo,
    removeTodo,
    addTodo,
    uncompletedCount,
    filteredTodos,
    rules,
    newTodo,
    newTodoName,
    hideCompleted,
    todos,
    id,
    editTask
  }
})
