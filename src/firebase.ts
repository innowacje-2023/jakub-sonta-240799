import { initializeApp } from 'firebase/app'
import { getAuth } from 'firebase/auth'
import { getFirestore } from 'firebase/firestore'

const firebaseConfig = {
  apiKey: 'AIzaSyAwvrXTnTpQlnHrbYpiEdRHqg4WORXJuEw',
  authDomain: 'jakub-sonta.firebaseapp.com',
  databaseURL: 'https://jakub-sonta-default-rtdb.europe-west1.firebasedatabase.app',
  projectId: 'jakub-sonta',
  storageBucket: 'jakub-sonta.appspot.com',
  messagingSenderId: '597635542322',
  appId: '1:597635542322:web:5e991b258b0b6a11273b2b',
  measurementId: 'G-WH5Y39EJK1'
}

export const firebase = initializeApp(firebaseConfig)
export const auth = getAuth(firebase)
export const db = getFirestore(firebase)
